<?php
class Usuarios_model extends CI_Model {

    public function __construct(){
            $this->load->database();
    }

    public function get_usuarios($id = FALSE){
        if ($id === FALSE){
            //$query = $this->db->get('usuarios');
            //return $query->result_array();
            return $this->db->query("select * from usuario_obtener();")->result_array();
        }
        else{
            //$query = $this->db->get_where('usuarios', array('id' => $id));
            //return $query->row_array();
            return $this->db->query("select * from usuario_obtener_id($id);")->result_array();
        }
        
    }

    public function create_usuarios(){
        $this->load->helper('url');
        /*
        $data = array(
            'cedula' => $this->input->post('cedula'),
            'nombre' => $this->input->post('nombre'),
            'apellido_1' => $this->input->post('apellido_1'),
            'apellido_2' => $this->input->post('apellido_2'),
            'telefono' => $this->input->post('telefono'),
            'tipo' => $this->input->post('tipo')
        );
        return $this->db->insert('usuarios', $data);
        */

        $in_cedula = $this->input->post('cedula');
        $in_nombre = $this->input->post('nombre');
        $in_apellido_1 = $this->input->post('apellido_1');
        $in_apellido_2 = $this->input->post('apellido_2');
        $in_telefono = $this->input->post('telefono');
        $in_tipo = $this->input->post('tipo');
        
        return $this->db->query("select usuario_crear($in_cedula, '$in_nombre', '$in_apellido_1', '$in_apellido_2', $in_telefono, $in_tipo);");
    }

    public function delete_usuarios(){
            /*
            $data = array( 'cedula' => $this->input->get('cedula') );
            return $this->db->delete('usuarios', $data);
            */
            $in_cedula = $this->input->get('cedula');
            return $this->db->query("select usuario_eliminar($in_cedula);");
        
    }

    public function update_usuarios(){
        $this->load->helper('url');

        $in_cedula = $this->input->post('cedula');
        $in_nombre = $this->input->post('nombre');
        $in_apellido_1 = $this->input->post('apellido_1');
        $in_apellido_2 = $this->input->post('apellido_2');
        $in_telefono = $this->input->post('telefono');
        $in_tipo = $this->input->post('tipo');
        
        return $this->db->query("select usuario_modificar($in_cedula, '$in_nombre', '$in_apellido_1', '$in_apellido_2', $in_telefono, $in_tipo);");
    }

    public function get_bomberos(){
        return $this->db->query("select * from usuario_obtener_tipo(1);")->result_array();
    }

}