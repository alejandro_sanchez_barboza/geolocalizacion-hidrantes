CREATE TABLE inspecciones(
    id SERIAL PRIMARY KEY,
    bombero int REFERENCES usuarios(id),
    hidrante int REFERENCES hidrantes(id),
    hidrante_nuevo varchar(30),
    completo bool DEFAULT false,
    accion int DEFAULT 0, -- Pendiente = 0, Instalacion = 1, Mantenimiento = 2, Ninguna = 3
    fecha_solicitud date not null,
    fecha_finalizacion date default null,
    geom public.geometry(Point,4326)
);

CREATE FUNCTION inspecciones_crear
(in_bombero int, in_hidrante int) 
RETURNS VOID AS
$$
BEGIN
  INSERT INTO inspecciones(bombero, hidrante, fecha_solicitud)
  VALUES(in_bombero, in_hidrante, current_date);
END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION inspecciones_crear_latlon
(in_bombero int, in_hidrante_nuevo varchar(30), in_latitud float, in_longitud float) 
RETURNS VOID AS
$$
BEGIN
  INSERT INTO inspecciones(bombero, hidrante, hidrante_nuevo, fecha_solicitud, geom)
  VALUES(in_bombero, 0, in_hidrante_nuevo, current_date, ST_SetSRID(ST_MakePoint(in_latitud, in_longitud), 4326));
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION inspecciones_eliminar
(in_id int) 
RETURNS VOID AS
$$
BEGIN
  DELETE FROM inspecciones WHERE id = in_id;
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION inspecciones_obtener
()
RETURNS TABLE (id int, bombero int, hidrante int, completo bool, accion int, fecha_solicitud date, fecha_finalizacion date) AS
$$
BEGIN
  RETURN QUERY 
  SELECT i.id, i.bombero, i.hidrante, i.completo, i.accion, i.fecha_solicitud, i.fecha_finalizacion 
  FROM inspecciones i;
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION inspecciones_obtener_id
(in_id int)
RETURNS TABLE (id int, bombero int, hidrante int, completo bool, accion int, fecha_solicitud date, fecha_finalizacion date) AS
$$
BEGIN
  RETURN QUERY
  SELECT i.id, i.bombero, i.hidrante, i.completo, i.accion, i.fecha_solicitud, i.fecha_finalizacion 
  FROM inspecciones i
  WHERE i.id = in_id;
END;
$$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION inspecciones_modificar
(in_id int, in_bombero int, in_hidrante int, in_accion int)
RETURNS VOID AS
$$
BEGIN
IF in_accion > 0 THEN
  UPDATE inspecciones
  SET bombero = in_bombero, hidrante = in_hidrante, accion = in_accion, completo = true, fecha_finalizacion = current_date 
  WHERE id = in_id;
ELSE
  UPDATE inspecciones
  SET bombero = in_bombero, hidrante = in_hidrante
  WHERE id = in_id;
END IF;
END;
$$
LANGUAGE 'plpgsql';

-- Trigger

CREATE OR REPLACE FUNCTION inspecciones_trigger_funcion()
RETURNS TRIGGER AS
$$
BEGIN
  IF new.accion > 0 THEN
    INSERT INTO solicitudes(inspeccion, fecha_solicitud)
    VALUES(new.id, current_date);
	RETURN NEW;
  END IF;
  RETURN NEW;
END;
$$
LANGUAGE 'plpgsql';

CREATE TRIGGER inspecciones_trigger AFTER UPDATE ON inspecciones
FOR EACH ROW EXECUTE PROCEDURE inspecciones_trigger_funcion();