CREATE TABLE hidrantes(
    id SERIAL PRIMARY KEY,
    nombre varchar(30) NOT NULL,
    calle int,
    avenida int,
    caudal int,
    geom public.geometry(Point,4326)
);

CREATE FUNCTION hidrantes_crear
(in_nombre varchar(30), in_calle int, avenida int, in_caudal int, in_geom public.geometry(Point,4326)) 
RETURNS VOID AS
$$
BEGIN
  INSERT INTO hidrantes(nombre, calle, avenida, caudal, geom)
  VALUES(in_nombre, in_calle, in_avenida, in_caudal, in_geom);
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION hidrantes_eliminar
(in_id int) 
RETURNS VOID AS
$$
BEGIN
  DELETE FROM hidrantes WHERE id = in_id;
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION hidrantes_obtener
()
RETURNS TABLE (id int, nombre varchar(30), calle int, avenida int, caudal int, geom public.geometry(Point,4326)) AS
$$
BEGIN
  RETURN QUERY 
  SELECT h.id, h.nombre, h.calle, h.avenida, h.caudal, h.geom 
  FROM hidrantes h;
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION hidrantes_obtener_id
(in_id int)
RETURNS TABLE (id int, nombre varchar(30), calle int, avenida int, caudal int, geom public.geometry(Point,4326)) AS
$$
BEGIN
  RETURN QUERY
  SELECT h.id, h.nombre, h.calle, h.avenida, h.caudal, h.geom 
  FROM hidrantes h
  WHERE h.id = in_id;
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION hidrantes_modificar
(in_id int, in_nombre varchar(30), in_calle int, in_avenida int, in_caudal int, in_geom public.geometry(Point, 4326)) 
RETURNS VOID AS
$$
BEGIN
  UPDATE hidrantes
  SET nombre = in_nombre, calle = in_calle, avenida = in_avenida, caudal = in_caudal, geom = in_geom
  WHERE id = in_id;
END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION hidrantes_modificar_latlon
(in_id int, in_nombre varchar(30), in_calle int, in_avenida int, in_caudal int, in_latitud float, in_longitud float) 
RETURNS VOID AS
$$
BEGIN
  UPDATE hidrantes
  SET nombre = in_nombre, calle = in_calle, avenida = in_avenida, caudal = in_caudal, geom = ST_SetSRID(ST_MakePoint(in_latitud, in_longitud), 4326)
  WHERE id = in_id;
END;
$$
LANGUAGE 'plpgsql';

-------------------------------------------

--DML 

INSERT INTO hidrantes(nombre, calle, avenida, caudal, geom)
VALUES
('Parque', 1, 5, 25,  '0101000020E61000007D08AA46AF0D55C0904AB1A371082440'), 
('Museo', 4, 2, 25,   '0101000020E61000000490DAC4C90D55C062A1D634EF082440'), 
('Taller', 8, 4, 25,  '0101000020E6100000DD41EC4CA10D55C0B7627FD93D092440'), 
('Galeria', 2, 8, 25, '0101000020E6100000DDEA39E97D0D55C0F12E17F19D082440'), 
('Launch', 7, 2, 25,  '0101000020E6100000C02154A9D90D55C0F33CB83B6B072440'), 
('Policia', 3, 9, 25, '0101000020E610000056F146E6910D55C09D8026C286072440'), 
('Fuente', 9, 6, 25,  '0101000020E61000009D6340F67A0D55C0D200DE02090A2440')